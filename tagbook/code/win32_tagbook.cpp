/**
 * TODO(rick):
 *
 **/

#include <stdio.h>
#include <windows.h>

#include "tagbook_common.h"
#include "win32_tagbook.h"

struct intern_node
{
    s32 RefCount;
    wchar_t *String;
    intern_node *Next;
};

struct intern_table
{
    intern_node *Buckets[512]; // NOTE(rick): Must be a power of 2
    intern_node *FreeList;

    memory_arena *Arena;
};

static void
InitializeInternTable(intern_table *Table, memory_arena *Arena)
{
    if(Table)
    {
        for(u32 BucketIndex = 0;
            BucketIndex < ArrayCount(Table->Buckets);
            ++BucketIndex)
        {
            *(Table->Buckets + BucketIndex) = {0};
        }
        Table->Arena = Arena;
    }
}

inline u32
HashString(wchar_t *String)
{
    // TODO(rick): BETTER HASH FUNCTION!
    // djb2 - http://www.cse.yorku.ca/~oz/hash.html
    u32 Hash = 5381;
    while(*String)
    {
        Hash = ((Hash << 5) + Hash) + *String++;
    }
    return(Hash);
}

static wchar_t *
PushString(intern_table *Table, wchar_t *String)
{
    wchar_t *Result = 0;

    _wcslwr(String);
    u32 Hash = HashString(String) & (ArrayCount(Table->Buckets) - 1);
    for(intern_node *Node = Table->Buckets[Hash];
        Node;
        Node = Node->Next)
    {
        if(wcscmp(String, Node->String) == 0)
        {
            ++Node->RefCount;
            Result = Node->String;
            break;
        }
    }

    if(!Result)
    {
        if(!Table->FreeList)
        {
            u32 FreeListSize = 256;
            Table->FreeList = PushArray(Table->Arena, FreeListSize, intern_node);

            intern_node *Node = Table->FreeList;
            for(u32 ListIndex = 0; ListIndex < (FreeListSize - 1); ++ListIndex)
            {
                Node[ListIndex].Next = &Node[ListIndex + 1];
            }
            Node[FreeListSize - 1].Next = 0;
        }

        size StringLength = wcslen(String);
        u32 StringBufferSize = (u32)(sizeof(wchar_t)*(StringLength+1));
        wchar_t *SaveString = (wchar_t *)VirtualAlloc(0, StringBufferSize, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
        Assert(SaveString);
        Copy(String, SaveString, StringBufferSize);

        intern_node *NewNode = Table->FreeList;
        Table->FreeList = NewNode->Next;
        NewNode->String = SaveString;
        NewNode->RefCount = 1;
        NewNode->Next = Table->Buckets[Hash];
        Table->Buckets[Hash] = NewNode;
        
        Result = SaveString;
    }

    return(Result);
}

static void
RemoveString(intern_table *Table, wchar_t *String)
{
    if(Table)
    {
        u32 Hash = HashString(String) & (ArrayCount(Table->Buckets) - 1);
        for(intern_node *Node = Table->Buckets[Hash];
            Node;
            Node = Node->Next)
        {
            if(Node->String == String)
            {
                --Node->RefCount;

                if(Node->RefCount < 0)
                {
                    // TODO(rick): Do we actually want to remove the node from
                    // the list and put it back on the free list? We probably
                    // won't be deleting strings very often so it probably
                    // doesn't matter if we leave a few unreference nodes in the
                    // table. Think about this!
                    Node->RefCount = 0;
                }

                break;
            }
        }
    }
}

static DWORD
WatchFileSystemForChanges(void *Data)
{
    // TODO(rick): Why does FileLastWrite trigger twice?
    HANDLE ChangeNotificationHandles[ChangeNotificationType_Count] = {0};
    ChangeNotificationHandles[ChangeNotificationType_FileAddDelete] = FindFirstChangeNotificationW(L".\\", TRUE, FILE_NOTIFY_CHANGE_FILE_NAME);
    ChangeNotificationHandles[ChangeNotificationType_DirectoryAddDelete] = FindFirstChangeNotificationW(L".\\", TRUE, FILE_NOTIFY_CHANGE_DIR_NAME);
    ChangeNotificationHandles[ChangeNotificationType_FileLastWrite] = FindFirstChangeNotificationW(L".\\", TRUE, FILE_NOTIFY_CHANGE_LAST_WRITE);
    if(ChangeNotificationHandles[ChangeNotificationType_FileAddDelete] &&
       ChangeNotificationHandles[ChangeNotificationType_DirectoryAddDelete] &&
       ChangeNotificationHandles[ChangeNotificationType_FileLastWrite])
    {
        printf("Watching for changes...\n");
        DWORD WaitStatus = 0;
        for(;;)
        {
            WaitStatus = WaitForMultipleObjects(ChangeNotificationType_Count, ChangeNotificationHandles, FALSE, INFINITE);

            switch(WaitStatus)
            {
                case WAIT_OBJECT_0 + ChangeNotificationType_FileAddDelete:
                case WAIT_OBJECT_0 + ChangeNotificationType_DirectoryAddDelete:
                case WAIT_OBJECT_0 + ChangeNotificationType_FileLastWrite:
                {
                    printf("Change within directory detected (%d)\n", WaitStatus - WAIT_OBJECT_0);
                    // TODO(rick): Can we be told which file changed?
                    // ReadDirectoryChangesW? Looks like this replaces the
                    // notification handle creation?
                    // NOTE(rick): Depending on how we want this thing to
                    // function maybe rebuilding the tree on every directory
                    // tree change is okay? This definitely won't work for an
                    // interactive GUI.
                    // https://medium.com/tresorit-engineering/how-to-get-notifications-about-file-system-changes-on-windows-519dd8c4fb01
                    if(FindNextChangeNotification(ChangeNotificationHandles[WaitStatus - WAIT_OBJECT_0]))
                    {
                        fprintf(stderr, "Set next notification\n");
                    }
                    else
                    {
                        fprintf(stderr, "Failed to set next notification\n");
                        break;
                    }
                } break;

                InvalidDefaultCase;
            }
        }
    }

    return(0);
}

struct tag_file_handle
{
    u32 Value;
};

struct tag_title_handle
{
    u32 Value;
};

struct tag_name_handle
{
    u32 Value;
};

struct tag_location
{
    tag_file_handle FileHandle;
    tag_title_handle TitleHandle;
    tag_name_handle NameHandle;
    u32 Pad;
};

struct tag_location_list
{
    tag_location Location[4];
    tag_location_list *Next;
};

struct tag_book_map
{
    tag_name_handle TagHandle;
    tag_location_list *Locations;
};

struct tag_book
{
    // IMPORTANT(rick): Index 0 is reserved for the NULL file/title/tag

    u32 MaxFileCount;
    u32 FileCount;
    wchar_t **Files;

    u32 MaxTitleCount;
    u32 TitleCount;
    wchar_t **Titles;

    u32 MaxTagCount;
    u32 TagCount;
    wchar_t **Tags;
    tag_book_map *TagMap;

    tag_location_list *TagLocationFreeList;
};

static memory_arena GlobalMemoryArena;
static intern_table GlobalInternTable;

inline tag_book
CreateTagBook(memory_arena *Arena)
{
    u32 InitialItemCount = 512;
    tag_book Result = {0};

    Result.FileCount = 1;
    Result.MaxFileCount = InitialItemCount;
    Result.Files = (wchar_t **)VirtualAlloc(0, sizeof(wchar_t *)*Result.MaxFileCount, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
    Assert(Result.Files);
    Result.Files[0] = 0;

    Result.TitleCount = 1;
    Result.MaxTitleCount = InitialItemCount;
    Result.Titles = (wchar_t **)VirtualAlloc(0, sizeof(wchar_t *)*Result.MaxTitleCount, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
    Assert(Result.Titles);
    Result.Titles[0] = 0;

    Result.TagCount = 1;
    Result.MaxTagCount = InitialItemCount;
    Result.Tags = (wchar_t **)VirtualAlloc(0, sizeof(wchar_t *)*Result.MaxTagCount, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
    Assert(Result.Tags);
    Result.Tags[0] = 0;

    Result.TagMap = (tag_book_map *)VirtualAlloc(0, sizeof(tag_book_map)*Result.MaxTagCount, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
    Assert(Result.TagMap);
    Result.TagMap[0] = {0};

    u32 LocationFreeListSize = 32;
    Result.TagLocationFreeList = PushArray(Arena, LocationFreeListSize, tag_location_list);
    Assert(Result.TagLocationFreeList);
    for(u32 FreeListIndex = 0; FreeListIndex < (LocationFreeListSize - 1); ++FreeListIndex)
    {
        Result.TagLocationFreeList[FreeListIndex].Next =
            &Result.TagLocationFreeList[FreeListIndex + 1];
    }
    Result.TagLocationFreeList[LocationFreeListSize - 1].Next = 0;

    return(Result);
}

inline tag_file_handle
TagBookAddTagFile(tag_book *TagBook, wchar_t *FileName)
{
    tag_file_handle Result = {0};

    if(TagBook)
    {
        for(u32 FileIndex = 0; FileIndex < TagBook->FileCount; ++FileIndex)
        {
            wchar_t *SavedFileName = *(TagBook->Files + FileIndex);
            if(SavedFileName == FileName)
            {
                Result.Value = FileIndex;
                // NOTE(rick): This should be an error? We can't have files with
                // the same name
                Assert(!"File with name already exists.");
                break;
            }
        }

        if(!Result.Value)
        {
            if((TagBook->FileCount + 1) >= TagBook->MaxFileCount)
            {
                u32 NewListSize = TagBook->MaxFileCount * 2;
                wchar_t **NewFileList = (wchar_t **)VirtualAlloc(0, sizeof(*NewFileList) * NewListSize, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
                if(NewFileList)
                {
                    wchar_t **OldFileList = TagBook->Files;

                    Copy(TagBook->Files, NewFileList, sizeof(*TagBook->Files)*TagBook->MaxFileCount);

                    TagBook->Files = NewFileList;

                    TagBook->MaxFileCount = NewListSize;

                    VirtualFree(OldFileList, 0, MEM_RELEASE);
                }
                else
                {
                    fprintf(stderr, "Couldn't allocate a new file list\n");
                }
            }

            TagBook->Files[TagBook->FileCount] = FileName;
            Result.Value = TagBook->FileCount++;
        }
    }

    return(Result);
}

inline tag_title_handle
TagBookAddTagTitle(tag_book *TagBook, wchar_t *Title)
{
    tag_title_handle Result = {0};

    if(TagBook)
    {
        for(u32 TitleIndex = 0; TitleIndex < TagBook->TitleCount; ++TitleIndex)
        {
            wchar_t *SavedTitle = *(TagBook->Titles + TitleIndex);
            if(SavedTitle == Title)
            {
                Result.Value = TitleIndex;
                break;
            }
        }

        if(!Result.Value)
        {
            if((TagBook->TitleCount + 1) >= TagBook->MaxTitleCount)
            {
                u32 NewListSize = TagBook->MaxTitleCount * 2;
                wchar_t **NewTitleList = (wchar_t **)VirtualAlloc(0, sizeof(*NewTitleList) * NewListSize, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
                if(NewTitleList)
                {
                    wchar_t **OldTitleList = TagBook->Titles;

                    Copy(TagBook->Titles, NewTitleList, sizeof(*TagBook->Titles)*TagBook->MaxTitleCount);

                    TagBook->Titles = NewTitleList;

                    TagBook->MaxTitleCount = NewListSize;

                    VirtualFree(OldTitleList, 0, MEM_RELEASE);
                }
                else
                {
                    fprintf(stderr, "Couldn't allocate a new title list\n");
                }
            }

            TagBook->Titles[TagBook->TitleCount] = Title;
            Result.Value = TagBook->TitleCount++;
        }
    }

    return(Result);
}

inline tag_name_handle
TagBookAddTagName(memory_arena *Arena, tag_book *TagBook, wchar_t *Tag,
                  tag_file_handle FileHandle, tag_title_handle TitleHandle)
{
    tag_name_handle Result = {0};

    if(TagBook)
    {
        for(u32 TagIndex = 0; TagIndex < TagBook->TagCount; ++TagIndex)
        {
            wchar_t *SavedTag = *(TagBook->Tags + TagIndex);
            if(SavedTag == Tag)
            {
                Result.Value = TagIndex;
                break;
            }
        }

        if(!Result.Value)
        {
            if((TagBook->TagCount + 1) >= TagBook->MaxTagCount)
            {
                u32 NewListSize = TagBook->MaxTagCount * 2;
                wchar_t **NewTagList = (wchar_t **)VirtualAlloc(0, sizeof(*NewTagList) * NewListSize, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
                tag_book_map *NewTagMap = (tag_book_map *)VirtualAlloc(0, sizeof(*NewTagMap) * NewListSize, MEM_RESERVE|MEM_COMMIT, PAGE_READWRITE);
                if(NewTagList && NewTagMap)
                {
                    wchar_t **OldTagList = TagBook->Tags;
                    Copy(TagBook->Tags, NewTagList, sizeof(*TagBook->Tags)*TagBook->MaxTagCount);
                    TagBook->Tags = NewTagList;
                    TagBook->MaxTagCount = NewListSize;
                    VirtualFree(OldTagList, 0, MEM_RELEASE);

                    tag_book_map *OldTagMap = TagBook->TagMap;
                    Copy(TagBook->TagMap, NewTagMap, sizeof(*TagBook->TagMap)*TagBook->MaxTagCount);
                    TagBook->TagMap = NewTagMap;
                    VirtualFree(OldTagMap, 0, MEM_RELEASE);
                }
                else
                {
                    fprintf(stderr, "Couldn't allocate a new tag list\n");
                }
            }

            u32 TagIndex = TagBook->TagCount++;
            TagBook->Tags[TagIndex] = Tag;
            Result.Value = TagIndex;
            TagBook->TagMap[TagIndex].TagHandle = Result;
        }

        if(Result.Value)
        {
            tag_book_map *TagMap = TagBook->TagMap + Result.Value;

            b32 LocationInserted = false;
            for(tag_location_list *LocationList = TagMap->Locations;
                LocationList && (LocationInserted == false);
                LocationList = LocationList->Next)
            {
                for(u32 LocationIndex = 0; LocationIndex < ArrayCount(LocationList->Location); ++LocationIndex)
                {
                    tag_location *Location = LocationList->Location + LocationIndex;
                    if((Location->FileHandle.Value == 0) &&
                       (Location->TitleHandle.Value == 0) &&
                       (Location->NameHandle.Value == 0))
                    {
                        Location->FileHandle = FileHandle;
                        Location->TitleHandle = TitleHandle;
                        Location->NameHandle = Result;

                        LocationInserted = true;
                        break;
                    }
                }
            }

            if(!LocationInserted)
            {
                if(!TagBook->TagLocationFreeList)
                {
                    u32 LocationFreeListSize = 32;
                    TagBook->TagLocationFreeList = PushArray(Arena, LocationFreeListSize, tag_location_list);
                    Assert(TagBook->TagLocationFreeList);
                    for(u32 FreeListIndex = 0; FreeListIndex < (LocationFreeListSize - 1); ++FreeListIndex)
                    {
                        TagBook->TagLocationFreeList[FreeListIndex].Next =
                            &TagBook->TagLocationFreeList[FreeListIndex + 1];
                    }
                    TagBook->TagLocationFreeList[LocationFreeListSize - 1].Next = 0;
                }

                tag_location_list *NewList = TagBook->TagLocationFreeList;
                TagBook->TagLocationFreeList = NewList->Next;

                NewList->Location[0].FileHandle = FileHandle;
                NewList->Location[0].TitleHandle = TitleHandle;
                NewList->Location[0].NameHandle = Result;

                NewList->Next = TagMap->Locations;
                TagMap->Locations = NewList;
            }
        }
    }

    return(Result);
}

inline wchar_t *
GetTagFileName(tag_book *TagBook, tag_file_handle TagFileHandle)
{
    wchar_t *Result = 0;
    Result = TagBook->Files[TagFileHandle.Value];
    return(Result);
}

inline wchar_t *
GetTagTitle(tag_book *TagBook, tag_title_handle TagTitleHandle)
{
    wchar_t *Result = 0;
    Result = TagBook->Titles[TagTitleHandle.Value];
    return(Result);
}

inline wchar_t *
GetTagName(tag_book *TagBook, tag_name_handle TagNameHandle)
{
    wchar_t *Result = 0;
    Result = TagBook->Tags[TagNameHandle.Value];
    return(Result);
}

static void
ParseTagFile(memory_arena *Arena, intern_table *InternTable, tag_book *TagBook, wchar_t *FileName)
{
    HANDLE FileHandle = CreateFileW(FileName, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, 0, 0);
    if(FileHandle != INVALID_HANDLE_VALUE)
    {
        DWORD FileSize = GetFileSize(FileHandle, 0);
        char *ReadBuffer = (char *)VirtualAlloc(0, sizeof(*ReadBuffer)*(FileSize + 1), MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE);
        if(ReadBuffer)
        {
            DWORD BytesRead = 0;
            if(ReadFile(FileHandle, ReadBuffer, FileSize, &BytesRead, 0) && (BytesRead == FileSize))
            {
                ReadBuffer[FileSize] = 0;

                s32 WideFileSize = MultiByteToWideChar(CP_UTF8, 0, ReadBuffer, FileSize+1, 0, 0);
                wchar_t *WideFile = (wchar_t *)VirtualAlloc(0, sizeof(*WideFile)*WideFileSize, MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE);
                if(WideFile)
                {
                    if(MultiByteToWideChar(CP_UTF8, 0, ReadBuffer, FileSize+1, WideFile, WideFileSize) == WideFileSize)
                    {
                        wchar_t *InternFileName = PushString(InternTable, FileName);
                        wchar_t *InternTitle = 0;
                        u32 InternTagCount = 0;
                        wchar_t *InternTagNames[512] = {0};

                        parse_mode ParseMode = ParseMode_Tags;

                        wchar_t *At = WideFile;
                        b32 Parsing = (*At != 0);
                        while(Parsing)
                        {
                            switch(ParseMode)
                            {
                                case ParseMode_Tags:
                                {
                                    if((At[0] == L'\n' && At[1] == L'\n') ||
                                       (At[0] == L'\r' && At[2] == L'\r'))
                                    {
                                        ParseMode = ParseMode_Title;
                                        ConsumeWhitespace(&At);
                                    }
                                    else if(At[0] == L'#')
                                    {
                                        ++At;
                                        wchar_t *End = At;
                                        while(!IsWhitespace(*End))
                                        {
                                            ++End;
                                        }

                                        if((InternTagCount + 1) < ArrayCount(InternTagNames))
                                        {
                                            wchar_t EndChar = *End;
                                            *End = 0;
                                            InternTagNames[InternTagCount++] = PushString(InternTable, At);
                                            *End = EndChar;
                                        }

                                        At = End;
                                    }
                                    else
                                    {
                                        ++At;
                                    }
                                } break;

                                case ParseMode_Title:
                                {
                                    if((At[0] == L'\n' && At[1] == L'\n') ||
                                       (At[0] == L'\r' && At[2] == L'\r'))
                                    {
                                        ParseMode = ParseMode_Body;
                                        ConsumeWhitespace(&At);
                                    }
                                    else if(!IsWhitespace(*At))
                                    {
                                        wchar_t *End = At;
                                        while(!IsEndOfLine(*End))
                                        {
                                            ++End;
                                        }

                                        wchar_t EndChar = *End;
                                        *End = 0;
                                        InternTitle = PushString(InternTable, At);
                                        *End = EndChar;

                                        At = End;
                                    }
                                    else
                                    {
                                        ++At;
                                    }
                                } break;

                                case ParseMode_Body:
                                {
#if 0
                                    wchar_t *End = At;
                                    while(*End != 0)
                                    {
                                        ++End;
                                    }

                                    wprintf(L"Body: %.*ws\n", (s32)(End - At), At);
                                    At = End;
#endif
                                    Parsing = false;
                                } break;

                                InvalidDefaultCase;
                            };

                            if(*At == 0)
                            {
                                Parsing = false;
                            }
                        }

                        tag_file_handle TagFileHandle = TagBookAddTagFile(TagBook, InternFileName);
                        tag_title_handle TagTitleHandle = TagBookAddTagTitle(TagBook, InternTitle);
                        for(u32 TagIndex = 0; TagIndex < InternTagCount; ++TagIndex)
                        {
                            wchar_t *Tag = InternTagNames[TagIndex];
                            TagBookAddTagName(Arena, TagBook, Tag, TagFileHandle, TagTitleHandle);
                        }
                    }
                    else
                    {
                        fprintf(stderr, "Failed to convert file\n");
                    }

                    VirtualFree(WideFile, 0, MEM_RELEASE);
                }
                else
                {
                    fprintf(stderr, "Failed to allocate wide file\n");
                }
            }
            else
            {
                fprintf(stderr, "ReadFile failed\n");
            }

            VirtualFree(ReadBuffer, 0, MEM_RELEASE);
        }
        else
        {
            fprintf(stderr, "Failed to create read buffer\n");
        }

        CloseHandle(FileHandle);
    }
    else
    {
        fprintf(stderr, "Couldn't open file\n");
    }
}

int
main(void)
{
    printf("Hello world\n");

    InitializeArena(&GlobalMemoryArena, Kilobytes(512));
    InitializeInternTable(&GlobalInternTable, &GlobalMemoryArena);

    tag_book TagBook_ = CreateTagBook(&GlobalMemoryArena);
    tag_book *TagBook = &TagBook_;

    WIN32_FIND_DATAW FindFileData = {0};
    HANDLE FindFileHandle = FindFirstFileW(L".\\*.txt", &FindFileData);
    if(FindFileHandle != INVALID_HANDLE_VALUE)
    {
        do
        {
            ParseTagFile(&GlobalMemoryArena, &GlobalInternTable, TagBook, FindFileData.cFileName);
        } while(FindNextFileW(FindFileHandle, &FindFileData));
    }

#if 0
    HANDLE WatchHandle = CreateThread(0, 0, WatchFileSystemForChanges, 0, 0, 0);
    CloseHandle(WatchHandle); // TODO(rick): Remove this if we care about the handle later

    wchar_t CommandBuffer[1024] = {0};
    while(fgetws(CommandBuffer, ArrayCount(CommandBuffer), stdin) != 0)
    {
    }
#endif

    return 0;
}
