@echo off

SET CompilerFlags=/nologo /Z7 /Od /Oi /fp:fast /FC /W4 /WX -wd4189 -wd4100 -wd4505 -wd4456 -D_CRT_SECURE_NO_WARNINGS
SET LinkerFlags=/incremental:no

IF NOT EXIST build mkdir build
pushd build

cl.exe %CompilerFlags% ..\tagbook\code\file_generator.cpp /link %LinkerFlags%
cl.exe %CompilerFlags% ..\tagbook\code\win32_tagbook.cpp /link %LinkerFlags%

popd
