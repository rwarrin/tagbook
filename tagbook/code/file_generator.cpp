#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

#include "tagbook_common.h"

char *Tags[] = {
    "#TK5GSzqmJC",
    "#KJrKddYSCs",
    "#vw6242qcKs",
    "#rbiwqIGsgj",
    "#fsPy84edOr",
    "#dLCE45BA3y",
    "#NWNCRRe3Y6",
    "#aUC1kvqppO",
    "#NsHao0zQCR",
    "#H44XHFWpPm",
    "#7m2qyh69Ok",
    "#rp82NniBDb",
    "#wa28kqyEgY",
    "#SMntSiO8Cw",
    "#SHbFdT4OOB",
    "#DhS4BwREAD",
    "#m4UVfgnAFU",
    "#iNuFLUUimx",
    "#o4CzvGYxjx",
    "#PYYJxAiMwp",
    "#5bwhrqkwLq",
    "#ZXQ1hb0l90",
    "#OJgMZmfvTu",
    "#UtQsou3Hfq",
    "#PXjfph0FUq",
    "#AHf51oEAFJ",
    "#KpWWtAdqhl",
    "#GlY3CV291J",
    "#ToVK3DXNh1",
    "#HhJZtcPoDj",
    "#5lpMOu3EUU",
    "#85uc9uV4oz",
};

int main(int ArgCount, char **Args)
{
    if(ArgCount != 2)
    {
        printf("Creates [count] .txt files in the current directory.\n");
        printf("Usage: %s [count]\n", Args[0]);
        return(-1);
    }

    s32 FilesToCreate = atoi(Args[1]);
    if(FilesToCreate < 0)
    {
        printf("[count] must be greater than 0. Entered %s\n", Args[1]);
        return(-2);
    }


    srand((unsigned int)time(0));
    SYSTEMTIME CurrentTime = {0};
    char FileName[256] = {0};
    char FileData[4096] = {0};
    for(s32 FileIndex = 0; FileIndex < FilesToCreate; )
    {
        GetSystemTime(&CurrentTime);
        snprintf(FileName, ArrayCount(FileName),
                 "%04d-%02d-%02d_%02d.%02d.%02d.%03d.txt",
                 CurrentTime.wYear,
                 CurrentTime.wMonth,
                 CurrentTime.wDay,
                 CurrentTime.wHour,
                 CurrentTime.wMinute,
                 CurrentTime.wSecond,
                 CurrentTime.wMilliseconds);

        char *FileDataAt = FileData;

        for(u32 TagIndex = 0; TagIndex < 5; ++TagIndex)
        {
            char *Tag = Tags[rand() % (ArrayCount(Tags) - 1)];
            FileDataAt += sprintf(FileDataAt, "%s ", Tag);
        }

        FileDataAt += sprintf(FileDataAt, "\n\nTitle: %s\n\n", FileName);

        for(u32 CharIndex = 0; CharIndex < 1024; ++CharIndex)
        {
            if(CharIndex && CharIndex % 80 == 0)
            {
                FileDataAt += sprintf(FileDataAt, "\n");
            }

            FileDataAt += sprintf(FileDataAt, "%c", ('a' + (rand() % 26)));
        }

        *FileDataAt++ = 0;

        HANDLE FileHandle = CreateFileA(FileName, GENERIC_WRITE, 0,
                                        0, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, 0);
        if(FileHandle != INVALID_HANDLE_VALUE)
        {
            DWORD BytesWritten = 0;
            DWORD BytesToWrite = (DWORD)(FileDataAt - FileData);
            WriteFile(FileHandle, FileData, BytesToWrite, &BytesWritten, 0);

            if(BytesWritten == BytesToWrite)
            {
                // NOTE(rick): Uhhh?
            }
            else
            {
                // NOTE(rick): Uhhh?
            }

            CloseHandle(FileHandle);
            ++FileIndex;
        }
    }

    return(0);
}
