#ifndef TAGBOOK_COMMON_H
#define TAGBOOK_COMMON_H

#include <stdint.h>

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef s32 b32;
typedef float f32;
typedef double f64;
typedef size_t size;
typedef intptr_t smm;
typedef uintptr_t umm;

#define Assert(Condition) do { if(!(Condition)) { *(s32 *)0 = 0; } } while(0)
#define InvalidDefaultCase default: { Assert(!"Invalid Default Case"); } break
#define InvalidCodePath Assert(!"Invalid Code Path")

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

#define Kilobytes(Size) (Size*1024LL)
#define Megabytes(Size) (Kilobytes(Size)*1024LL)
#define Gigabytes(Size) (Megabytes(Size)*1024LL)

struct memory_block
{
    size Used;
    size Size;
    u8 *Base;

    memory_block *NextBlock;
};

struct memory_arena
{
    memory_block *CurrentBlock;

    // TODO(rick): Do we need temporary arenas?
};

static memory_block *
AllocateMemoryBlock(size Size)
{
    memory_block *Result = 0;
    u8 *Memory = (u8 *)VirtualAlloc(0, Size, MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE);
    Assert(Memory);

    Result = (memory_block *)Memory;
    Result->Base = (u8 *)(Result + 1);
    Result->Size = Size;
    Result->Used = 0;
    Result->NextBlock = 0;
    return(Result);
}

static void
InitializeArena(memory_arena *Arena, size Size)
{
    Assert(Arena);
    Arena->CurrentBlock = AllocateMemoryBlock(Size);
}

#define PushStruct(Arena, type) (type *)_PushSize(Arena, sizeof(type))
#define PushArray(Arena, count, type) (type *)_PushSize(Arena, sizeof(type)*count)
#define PushSize(Arena, Size) _PushSize(Arena, Size)

static void *
_PushSize(memory_arena *Arena, size Size)
{
    // TODO(rick): Implement alignment

    void *Result = 0;
    if((Arena->CurrentBlock->Used + Size) > Arena->CurrentBlock->Size)
    {
        size MinimumBlockSize = Kilobytes(512);
        size NewSize = MinimumBlockSize;
        if(Size > NewSize)
        {
            NewSize = Size*2;
        }

        memory_block *NewBlock = AllocateMemoryBlock(NewSize);
        NewBlock->NextBlock = Arena->CurrentBlock;
        Arena->CurrentBlock = NewBlock;
    }

    Assert(Arena->CurrentBlock);
    Assert((Arena->CurrentBlock->Used + Size) <= Arena->CurrentBlock->Size);
    Result = (void *)(Arena->CurrentBlock->Base + Arena->CurrentBlock->Used);
    Arena->CurrentBlock->Used += Size;

    return(Result);
}

inline void
Copy(void *SrcIn, void *DestIn, u32 Size)
{
    u8 *Dest = (u8 *)DestIn;
    u8 *Src = (u8 *)SrcIn;
    while(Size--)
    {
        *Dest++ = *Src++;
    }
}

inline b32
IsEndOfLine(wchar_t C)
{
    b32 Result = ( (C == L'\r') ||
                   (C == L'\n') ||
                   (C == 0) );
    return(Result);
}

inline b32
IsWhitespace(wchar_t C)
{
    b32 Result = ( (C == L' ') ||
                   (C == L'\r') ||
                   (C == L'\n') ||
                   (C == L'\t') ||
                   (C == 0) );
    return(Result);
}

inline void
ConsumeWhitespace(wchar_t **Buffer)
{
    wchar_t *At = *Buffer;
    while(IsWhitespace(*At))
    {
        ++At;
    }
    *Buffer = At;
}
#endif
