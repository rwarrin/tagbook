#ifndef WIN32_TAGBOOK_H
#define WIN32_TAGBOOK_H

enum parse_mode
{
    ParseMode_None,

    ParseMode_Tags,
    ParseMode_Title,
    ParseMode_Body,

    ParseMode_Count
};

enum change_notification_type
{
    ChangeNotificationType_FileAddDelete = 0,
    ChangeNotificationType_DirectoryAddDelete,
    ChangeNotificationType_FileLastWrite,

    ChangeNotificationType_Count,
};

#endif
