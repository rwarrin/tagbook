@echo off

SET loc=%cd:~0,-13%

SUBST T: /D
SUBST T: %loc%
pushd T:

call "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" x64
start D:\Development\Vim\vim80\gvim.exe

cls
